package com.paiwaddev.jobs.pwtodoapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TODOApp: Application() {
}