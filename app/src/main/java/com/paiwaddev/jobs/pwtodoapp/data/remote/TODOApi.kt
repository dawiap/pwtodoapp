package com.paiwaddev.jobs.pwtodoapp.data.remote

import com.google.gson.JsonObject
import com.paiwaddev.jobs.pwtodoapp.data.model.LogoutResponse
import com.paiwaddev.jobs.pwtodoapp.data.model.task.Data
import com.paiwaddev.jobs.pwtodoapp.data.model.task.AllTaskResponse
import com.paiwaddev.jobs.pwtodoapp.data.model.task.TaskResponse
import com.paiwaddev.jobs.pwtodoapp.data.model.user.UserResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.*

interface TODOApi {
    @POST("/user/register")
    fun register(@Body body: JsonObject): Observable<UserResponse>

    @POST("/user/login")
    fun login(@Body body: JsonObject): Observable<UserResponse>

    @POST("/user/logout")
    fun logout(
        @Header("Authorization") token: String
    ): Observable<LogoutResponse>

    @GET("/task")
    fun getAllTodo(
        @Header("Authorization") token: String,
    ): Observable<AllTaskResponse>

    @GET("/task/{id}")
    fun getTodo(
        @Header("Authorization") token: String,
        @Path("id") id: String
    ): Observable<TaskResponse>

    @POST("/task")
    fun createTask(
            @Header("Authorization") token: String,
            @Body body: JsonObject
    ): Observable<TaskResponse>

    @DELETE("/task/{id}")
    fun deleteTask(
            @Header("Authorization") token: String,
            @Path("id") id: String
    ): Observable<TaskResponse>

    @PUT("/task/{id}")
    fun updateTask(
            @Header("Authorization") token: String,
            @Path("id") id: String,
            @Body body: JsonObject
    ): Observable<TaskResponse>

}