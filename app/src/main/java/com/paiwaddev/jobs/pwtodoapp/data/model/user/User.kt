package com.paiwaddev.jobs.pwtodoapp.data.model.user


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class User(
    @SerializedName("age")
    val age: Int,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("_id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("updatedAt")
    val updatedAt: String,
    @SerializedName("__v")
    val v: Int
): Serializable