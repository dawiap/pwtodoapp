package com.paiwaddev.jobs.pwtodoapp.data.repository

import com.google.gson.JsonObject
import com.paiwaddev.jobs.pwtodoapp.data.model.task.Data
import com.paiwaddev.jobs.pwtodoapp.data.model.task.AllTaskResponse
import com.paiwaddev.jobs.pwtodoapp.data.model.task.TaskResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.Body

interface TaskRepository {
    fun getAllTask(token: String): Observable<AllTaskResponse>

    fun getTask(token: String, _id: String): Observable<TaskResponse>

    fun createTask(token: String, body: JsonObject): Observable<TaskResponse>

    fun deleteTask(token: String, _id: String): Observable<TaskResponse>

    fun updateTask(token: String, _id: String, body: JsonObject): Observable<TaskResponse>
}