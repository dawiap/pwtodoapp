package com.paiwaddev.jobs.pwtodoapp.ui.adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.paiwaddev.jobs.pwtodoapp.data.model.task.Data

@BindingAdapter("app:taskList")
fun setTaskList(rcv: RecyclerView, items: List<Data>?){
    items?.let { data ->
        (rcv.adapter as TaskListAdapter).apply {
            differ.submitList(data)
            notifyDataSetChanged()
        }
    }
}