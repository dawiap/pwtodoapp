package com.paiwaddev.jobs.pwtodoapp.data.model.task


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("completed")
    val completed: Boolean,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("_id")
    val id: String,
    @SerializedName("owner")
    val owner: String,
    @SerializedName("updatedAt")
    val updatedAt: String,
    @SerializedName("__v")
    val v: Int
)