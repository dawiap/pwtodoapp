package com.paiwaddev.jobs.pwtodoapp.data

import com.google.gson.JsonObject
import com.paiwaddev.jobs.pwtodoapp.data.model.LogoutResponse
import com.paiwaddev.jobs.pwtodoapp.data.model.user.UserResponse
import com.paiwaddev.jobs.pwtodoapp.data.remote.TODOApi
import com.paiwaddev.jobs.pwtodoapp.data.repository.UserRepository
import io.reactivex.rxjava3.core.Observable

class UserRepositoryImpl(private val remote: TODOApi): UserRepository {
    override fun userRegister(body: JsonObject): Observable<UserResponse> {
        return remote.register(body)
    }

    override fun userLogin(body: JsonObject): Observable<UserResponse> {
        return remote.login(body)
    }

    override fun userLogout(token: String): Observable<LogoutResponse> {
        return remote.logout(token)
    }
}