package com.paiwaddev.jobs.pwtodoapp.data.model.user


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UserResponse(
    @SerializedName("token")
    val token: String,
    @SerializedName("user")
    val user: User
): Serializable