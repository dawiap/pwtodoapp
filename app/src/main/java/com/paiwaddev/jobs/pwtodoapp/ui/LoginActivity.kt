package com.paiwaddev.jobs.pwtodoapp.ui

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwaddev.jobs.pwtodoapp.R
import com.paiwaddev.jobs.pwtodoapp.databinding.ActivityLoginBinding
import com.paiwaddev.jobs.pwtodoapp.viewmodel.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        binding.viewModel = loginViewModel
        binding.lifecycleOwner = this

        val sharedPref = getSharedPreferences("Session", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token",null)
        if(!token.isNullOrEmpty())
            startActivity(Intent(this,MainActivity::class.java))

        loginViewModel.message.observe(this,SingleEventObserver{
            Toast.makeText(this,it,Toast.LENGTH_LONG).show()
        })

        val progress = ProgressDialog(this)
        loginViewModel.loading.observe(this,SingleEventObserver{
            if(it) {
                progress.show()
            }else {
                progress.dismiss()
            }
        })
    }
}