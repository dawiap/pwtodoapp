package com.paiwaddev.jobs.pwtodoapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwaddev.jobs.pwtodoapp.data.model.task.Data
import com.paiwaddev.jobs.pwtodoapp.databinding.TaskItemBinding

class TaskListAdapter: RecyclerView.Adapter<TaskListAdapter.TaskViewHolder>() {

    lateinit var clickListener: ((View, Data) -> Unit)

    inner class TaskViewHolder(private val binding: TaskItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Data, clickListener: (View, Data) -> Unit) {
            binding.task = item
            binding.imageViewMore.setOnClickListener {
                clickListener(binding.root, item)
            }
        }
    }

    private val callBack = object : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem.id == newItem.id
        }

    }

    val differ = AsyncListDiffer(this, callBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(
                TaskItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(differ.currentList[position], clickListener)
    }

    override fun getItemCount(): Int = differ.currentList.size
}