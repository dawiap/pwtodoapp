package com.paiwaddev.jobs.pwtodoapp.di

import com.paiwaddev.jobs.pwtodoapp.BuildConfig
import com.paiwaddev.jobs.pwtodoapp.data.TaskRepositoryImpl
import com.paiwaddev.jobs.pwtodoapp.data.UserRepositoryImpl
import com.paiwaddev.jobs.pwtodoapp.data.remote.TODOApi
import com.paiwaddev.jobs.pwtodoapp.data.repository.TaskRepository
import com.paiwaddev.jobs.pwtodoapp.data.repository.UserRepository
import com.paiwaddev.jobs.pwtodoapp.ui.adapter.TaskListAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class MainModule {

    @Provides
    fun provideRetrofit(): TODOApi{
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.BASE_URL)
            .build()
        return retrofit.create(TODOApi::class.java)
    }

    @Provides
    fun provideUserRepository(remoteSource: TODOApi): UserRepository{
        return UserRepositoryImpl(remoteSource)
    }

    @Provides
    fun provideTaskRepository(remoteSource: TODOApi): TaskRepository{
        return TaskRepositoryImpl(remoteSource)
    }

    @Provides
    fun provideTaskListAdapter(): TaskListAdapter{
        return TaskListAdapter()
    }
}