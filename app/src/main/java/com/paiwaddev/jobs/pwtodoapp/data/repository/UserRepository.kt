package com.paiwaddev.jobs.pwtodoapp.data.repository

import com.google.gson.JsonObject
import com.paiwaddev.jobs.pwtodoapp.data.model.LogoutResponse
import com.paiwaddev.jobs.pwtodoapp.data.model.user.UserResponse
import io.reactivex.rxjava3.core.Observable

interface UserRepository {
    fun userRegister(body: JsonObject): Observable<UserResponse>

    fun userLogin(body: JsonObject): Observable<UserResponse>

    fun userLogout(token: String): Observable<LogoutResponse>
}