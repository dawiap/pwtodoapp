package com.paiwaddev.jobs.pwtodoapp.ui

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwaddev.jobs.pwtodoapp.R
import com.paiwaddev.jobs.pwtodoapp.databinding.ActivityRegisterBinding
import com.paiwaddev.jobs.pwtodoapp.viewmodel.RegisterViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {

    private val registerViewModel: RegisterViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityRegisterBinding>(this, R.layout.activity_register)
        binding.viewModel = registerViewModel
        binding.lifecycleOwner = this

        registerViewModel.message.observe(this, SingleEventObserver{
            Toast.makeText(this,it, Toast.LENGTH_LONG).show()
        })

        val progress = ProgressDialog(this)
        registerViewModel.loading.observe(this, SingleEventObserver{
            if(it) {
                progress.show()
            }else {
                progress.dismiss()
            }
        })
    }
}