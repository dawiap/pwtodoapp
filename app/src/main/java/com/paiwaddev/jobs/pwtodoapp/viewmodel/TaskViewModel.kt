package com.paiwaddev.jobs.pwtodoapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwaddev.jobs.pwtodoapp.data.model.task.Data
import com.paiwaddev.jobs.pwtodoapp.data.model.task.AllTaskResponse
import com.paiwaddev.jobs.pwtodoapp.data.model.task.TaskResponse
import com.paiwaddev.jobs.pwtodoapp.data.repository.TaskRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class TaskViewModel@Inject constructor(private val repository: TaskRepository): ViewModel() {
    val inputDescription = MutableLiveData<String?>()

    private val _taskResponse = MutableLiveData<AllTaskResponse>()
    val allTaskResponse: LiveData<AllTaskResponse> = _taskResponse

    private val _task = MutableLiveData<SingleEvent<TaskResponse>>()
    val task: LiveData<SingleEvent<TaskResponse>> = _task

    private val _message = MutableLiveData<SingleEvent<String>>()
    val message: LiveData<SingleEvent<String>> = _message

    private val _loading = MutableLiveData<SingleEvent<Boolean>>()
    val loading: LiveData<SingleEvent<Boolean>> = _loading

    private val compositeDisposable = CompositeDisposable()

    fun getAllTask(token: String){
        _loading.postValue(SingleEvent(true))
        val disposable = repository.getAllTask(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ taskResponse ->
                    _loading.postValue(SingleEvent(false))
                    _taskResponse.postValue(taskResponse)

                },{
                    _loading.postValue(SingleEvent(false))
                    _message.postValue(SingleEvent((it.message.toString())))
                })
        compositeDisposable.add(disposable)
    }

    fun getTask(token: String, _id: String){
        _loading.postValue(SingleEvent(true))
        val disposable = repository.getTask(token,_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ task ->
                    _loading.postValue(SingleEvent(false))
                    _task.postValue(SingleEvent(task))

                },{
                    _loading.postValue(SingleEvent(false))
                    _message.postValue(SingleEvent((it.message.toString())))
                })
        compositeDisposable.add(disposable)
    }

    fun createTask(token: String){
        if(inputDescription.value.isNullOrEmpty()){
            _message.postValue(SingleEvent("Please enter description"))
            return
        }

        val body = JsonObject()
        body.addProperty("description",inputDescription.value.toString())

        _loading.postValue(SingleEvent(true))
        val disposable = repository.createTask(token,body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ task ->
                    //_loading.postValue(SingleEvent(false))
                    getAllTask(token)
                    inputDescription.postValue(null)
                },{
                    _loading.postValue(SingleEvent(false))
                    _message.postValue(SingleEvent((it.message.toString())))
                })
        compositeDisposable.add(disposable)
    }

    fun updateTask(token: String,_id: String,isUpdateDescriptionOnly: Boolean, isComplete: Boolean){

        if(inputDescription.value.isNullOrEmpty()){
            _message.postValue(SingleEvent("Please enter description"))
            return
        }

        val body = JsonObject()
        if(!isUpdateDescriptionOnly)
            body.addProperty("completed",isComplete)
        body.addProperty("description",inputDescription.value.toString())

        _loading.postValue(SingleEvent(true))
        val disposable = repository.updateTask(token,_id,body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    getAllTask(token)
                    inputDescription.postValue(null)
                },{
                    _loading.postValue(SingleEvent(false))
                    _message.postValue(SingleEvent((it.message.toString())))
                })
        compositeDisposable.add(disposable)
    }

    fun deleteTask(token: String,_id: String){
        _loading.postValue(SingleEvent(true))
        val disposable = repository.deleteTask(token,_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    getAllTask(token)
                },{
                    _loading.postValue(SingleEvent(false))
                    _message.postValue(SingleEvent((it.localizedMessage.toString())))
                })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}