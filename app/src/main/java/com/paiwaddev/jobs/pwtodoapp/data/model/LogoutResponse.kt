package com.paiwaddev.jobs.pwtodoapp.data.model


import com.google.gson.annotations.SerializedName

data class LogoutResponse(
    @SerializedName("success")
    val success: Boolean
)