package com.paiwaddev.jobs.pwtodoapp.ui

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.MenuRes
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwaddev.jobs.pwtodoapp.R
import com.paiwaddev.jobs.pwtodoapp.data.model.task.Data
import com.paiwaddev.jobs.pwtodoapp.data.model.user.UserResponse
import com.paiwaddev.jobs.pwtodoapp.databinding.ActivityMainBinding
import com.paiwaddev.jobs.pwtodoapp.databinding.AddTaskBottomLayoutBinding
import com.paiwaddev.jobs.pwtodoapp.databinding.UpdateTaskBottomLayoutBinding
import com.paiwaddev.jobs.pwtodoapp.ui.adapter.TaskListAdapter
import com.paiwaddev.jobs.pwtodoapp.viewmodel.LoginViewModel
import com.paiwaddev.jobs.pwtodoapp.viewmodel.TaskViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val taskViewModel: TaskViewModel by viewModels()

    private val loginViewModel: LoginViewModel by viewModels()

    @Inject lateinit var taskListAdapter: TaskListAdapter

    private lateinit var addTaskBottomLayoutBinding: AddTaskBottomLayoutBinding
    private lateinit var updateTaskBottomLayoutBinding: UpdateTaskBottomLayoutBinding

    private lateinit var addTaskBottomDialog: BottomSheetDialog
    private lateinit var updateTaskBottomDialog: BottomSheetDialog

    private lateinit var sharedPref: SharedPreferences
    private var token: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.viewModel = taskViewModel
        binding.lifecycleOwner = this

        testFilterArray()

        sharedPref = getSharedPreferences("Session",Context.MODE_PRIVATE)
        token = sharedPref.getString("token",null)

        token?.let { taskViewModel.getAllTask(it) }

        binding.rcvTask.adapter = taskListAdapter
        taskListAdapter.clickListener = {view, data ->  onItemTaskClick(view,data)}

        setupAddTaskBottomLayout()

        binding.fabAddTask.setOnClickListener {
            addTaskBottomDialog.show()
        }

        observeState()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logout -> {
                loginViewModel.onUserLogout(token!!)
                loginViewModel.logoutResponse.observe(this, {
                    if(it.success) {
                        sharedPref.edit().clear().apply()
                        finish()
                    }
                })
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun observeState(){
        taskViewModel.message.observeMessage()
        loginViewModel.message.observeMessage()
        taskViewModel.loading.observeLoading()
        loginViewModel.loading.observeLoading()
    }

    private fun LiveData<SingleEvent<String>>.observeMessage() {
        this.observe(this@MainActivity, SingleEventObserver {
            Toast.makeText(this@MainActivity, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun LiveData<SingleEvent<Boolean>>.observeLoading(){
        val progress = ProgressDialog(this@MainActivity)
        this.observe(this@MainActivity, SingleEventObserver{
            if(it) {
                progress.show()
            }else {
                progress.dismiss()
                addTaskBottomDialog.dismiss()
            }
        })
    }

    private fun setupAddTaskBottomLayout() {
        addTaskBottomLayoutBinding =
                AddTaskBottomLayoutBinding.inflate(layoutInflater, null, false)
        addTaskBottomDialog = BottomSheetDialog(this).apply {
            setContentView(addTaskBottomLayoutBinding.root)
        }
        addTaskBottomLayoutBinding.viewModel = taskViewModel
        addTaskBottomLayoutBinding.addButton.setOnClickListener {
            taskViewModel.createTask(token!!)
        }
    }

    private fun setupUpdateTaskBottomLayout(task: Data) {
        updateTaskBottomLayoutBinding =
                UpdateTaskBottomLayoutBinding.inflate(layoutInflater, null, false)
        updateTaskBottomDialog = BottomSheetDialog(this).apply {
            setContentView(updateTaskBottomLayoutBinding.root)
        }
        taskViewModel.inputDescription.value = task.description
        updateTaskBottomLayoutBinding.viewModel = taskViewModel
        updateTaskBottomLayoutBinding.task = task
        updateTaskBottomLayoutBinding.addButton.setOnClickListener {
            taskViewModel.updateTask(
                    token!!,
                    task.id,
                    updateTaskBottomLayoutBinding.radioButtonComplete.isChecked == task.completed,
                    updateTaskBottomLayoutBinding.radioButtonComplete.isChecked
            )
            updateTaskBottomDialog.dismiss()
        }
    }

    private fun onItemTaskClick(v: View, task: Data){
        showOptionMenu(v,R.menu.manage_menu,task)
    }

    private fun showOptionMenu(v: View, @MenuRes menuRes: Int, task: Data) {
        val popup = android.widget.PopupMenu(this, v)
        popup.menuInflater.inflate(menuRes, popup.menu)

        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            when(menuItem.itemId){
                R.id.edit->{
                    taskViewModel.getTask(token!!,task.id)
                    taskViewModel.task.observe(this,SingleEventObserver{
                        setupUpdateTaskBottomLayout(task)
                        updateTaskBottomDialog.show()
                    })
                }
                R.id.delete ->{
                    taskViewModel.deleteTask(token!!,task.id)
                }
            }
            false
        }
        popup.show()
    }

    override fun onBackPressed() {
        Log.e("TAG","Back")
    }


    private fun testFilterArray(){
        val array1 = listOf(1,2,3,4,5,6,7,8,9,10)
        val array2 = listOf(1,2,7,7,11)
        println(filterArray(array1,array2))
    }

    private fun filterArray(arr1: List<Int>, arr2: List<Int>): List<Int>{
        val array = ArrayList<Int>()
        for(i in arr1.indices){
            for(j in arr2.indices){
                if(arr1[i] == arr2[j]) {
                    array.add(arr1[i])
                    break
                }
            }
        }
        return array
    }
}