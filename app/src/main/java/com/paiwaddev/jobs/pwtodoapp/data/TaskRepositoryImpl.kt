package com.paiwaddev.jobs.pwtodoapp.data

import com.google.gson.JsonObject
import com.paiwaddev.jobs.pwtodoapp.data.model.task.Data
import com.paiwaddev.jobs.pwtodoapp.data.model.task.AllTaskResponse
import com.paiwaddev.jobs.pwtodoapp.data.model.task.TaskResponse
import com.paiwaddev.jobs.pwtodoapp.data.remote.TODOApi
import com.paiwaddev.jobs.pwtodoapp.data.repository.TaskRepository
import io.reactivex.rxjava3.core.Observable

class TaskRepositoryImpl(private val remote: TODOApi): TaskRepository {
    override fun getAllTask(token: String): Observable<AllTaskResponse> {
        return remote.getAllTodo(token)
    }

    override fun getTask(token: String, _id: String): Observable<TaskResponse> {
        return remote.getTodo(token,_id)
    }

    override fun createTask(token: String, body: JsonObject): Observable<TaskResponse> {
        return remote.createTask(token, body)
    }

    override fun deleteTask(token: String, _id: String): Observable<TaskResponse> {
        return remote.deleteTask(token,_id)
    }

    override fun updateTask(token: String, _id: String, body: JsonObject): Observable<TaskResponse> {
        return remote.updateTask(token,_id,body)
    }
}