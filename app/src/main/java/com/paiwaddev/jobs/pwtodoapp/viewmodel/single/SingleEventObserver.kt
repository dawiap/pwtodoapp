package com.paiwad.myapp.paiwadpos.util

import androidx.lifecycle.Observer

class SingleEventObserver<T>(private val onEventUnconsumedContent: (T) -> Unit) :
    Observer<SingleEvent<T>> {
    override fun onChanged(event: SingleEvent<T>?) {
        event?.consume()?.run(onEventUnconsumedContent)
    }
}