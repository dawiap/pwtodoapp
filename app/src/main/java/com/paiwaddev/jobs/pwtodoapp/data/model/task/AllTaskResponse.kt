package com.paiwaddev.jobs.pwtodoapp.data.model.task


import com.google.gson.annotations.SerializedName

data class AllTaskResponse(
    @SerializedName("count")
    val count: Int,
    @SerializedName("data")
    val data: List<Data>
)