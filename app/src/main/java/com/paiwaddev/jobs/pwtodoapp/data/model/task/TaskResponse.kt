package com.paiwaddev.jobs.pwtodoapp.data.model.task


import com.google.gson.annotations.SerializedName

data class TaskResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("success")
    val success: Boolean
)