package com.paiwaddev.jobs.pwtodoapp.viewmodel

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwaddev.jobs.pwtodoapp.data.model.LogoutResponse
import com.paiwaddev.jobs.pwtodoapp.ui.MainActivity
import com.paiwaddev.jobs.pwtodoapp.ui.RegisterActivity
import com.paiwaddev.jobs.pwtodoapp.data.model.user.UserResponse
import com.paiwaddev.jobs.pwtodoapp.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val repository: UserRepository): ViewModel() {

    val inputEmail = MutableLiveData<String>()
    val inputPass = MutableLiveData<String>()

    private val _logoutResponse = MutableLiveData<LogoutResponse>()
    val logoutResponse: LiveData<LogoutResponse> = _logoutResponse

    private val _message = MutableLiveData<SingleEvent<String>>()
    val message: LiveData<SingleEvent<String>> = _message

    private val _loading = MutableLiveData<SingleEvent<Boolean>>()
    val loading: LiveData<SingleEvent<Boolean>> = _loading

    private val compositeDisposable = CompositeDisposable()

    fun onUserLogin(v: View){
        if(inputEmail.value.isNullOrEmpty() || inputPass.value.isNullOrEmpty()) {
            _message.postValue(SingleEvent("Please enter email or password"))
            return
        }

        val body = JsonObject()
        body.addProperty("email",inputEmail.value.toString())
        body.addProperty("password",inputPass.value.toString())

        _loading.postValue(SingleEvent(true))
        val disposable = repository.userLogin(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ userResponse ->
                _loading.postValue(SingleEvent(false))
                userResponse?.let {
                    val sharedPref  = v.context.getSharedPreferences("Session", Context.MODE_PRIVATE)
                    val editor = sharedPref.edit()
                    editor.putString("token",it.token)
                    editor.commit()
                    v.context.startActivity(Intent(v.context, MainActivity::class.java))
                }

            },{
                _loading.postValue(SingleEvent(false))
                _message.postValue(SingleEvent((it.message.toString())))
            })
        compositeDisposable.add(disposable)
    }

    fun onUserLogout(token: String){
        _loading.postValue(SingleEvent(true))
        val disposable = repository.userLogout(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.postValue(SingleEvent(false))
                    _logoutResponse.postValue(it)
                },{
                    _loading.postValue(SingleEvent(false))
                    _message.postValue(SingleEvent((it.message.toString())))
                })
        compositeDisposable.add(disposable)
    }

    fun goToRegister(v: View){
        v.context.startActivity(Intent(v.context, RegisterActivity::class.java))
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}